import React from "react";
import { Navbar, Container } from "react-bootstrap";
export default function Footer() {
    return (
        <Navbar fixed="bottom" className="pb-1 footer">
            <Container className="text-center">
                <p className="mx-auto">Copyright © 2018 Run for Earth with Ms. Philippines Earth </p>
            </Container>
        </Navbar>
    );
}
