import "../App.css";
import axios from "axios";
import { useEffect, useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const AdminView = () => {
  const classes = useStyles();
  const [product, setProduct] = useState([]);
  const [search, setSearch] = useState("");

  const getProductData = async () => {
    try {
      const data = await axios.get(
        "https://runforearth-be.herokuapp.com/users"
      );
      console.log(data.data);
      setProduct(data.data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getProductData();
  }, []);
  return (
    <div className="App">
      <h1>List of Participants</h1>
      <input
        type="text"
        placeholder="Search here"
        onChange={(e) => {
          setSearch(e.target.value);
        }}
      />

      {/* {product
        .filter((item) => {
          if (search == "") {
            return item;
          } else if (item.name.toLowerCase().includes(search.toLowerCase())) {
            return item;
          }
        })
        .map((item) => {
          return (
            <p>
              {item.name} - {item.price}
            </p>
          );
        })} */}

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>First Name</StyledTableCell>
              <StyledTableCell>Last Name</StyledTableCell>
              <StyledTableCell>Birthday</StyledTableCell>
              <StyledTableCell>Gender</StyledTableCell>
              <StyledTableCell>Address</StyledTableCell>
              <StyledTableCell>Email Address</StyledTableCell>
              <StyledTableCell>Contact No.</StyledTableCell>
              <StyledTableCell>Shirt Size</StyledTableCell>
              <StyledTableCell>Date Registered</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {product
              .filter((item) => {
                if (search == "") {
                  return item;
                } else if (
                  item.firstName.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.lastName.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.birthday.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.gender.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                }else if (
                  item.address.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.email.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.contactNo.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.shirtSize.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                } else if (
                  item.dateRegistered.toLowerCase().includes(search.toLowerCase())
                ) {
                  return item;
                }
              }) 
              .map((item) => {
                return (
                  <StyledTableRow key={item.id}>
                    <StyledTableCell component="th" scope="row">
                      {item.firstName}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.lastName}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.birthday}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.gender}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.address}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.email}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.contactNo}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.shirtSize}
                    </StyledTableCell>
                    <StyledTableCell component="th" scope="row">
                      {item.dateRegistered}
                    </StyledTableCell>
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default AdminView;
