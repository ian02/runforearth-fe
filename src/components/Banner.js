import React from 'react';
import {   Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'
 
 
const Banner = ({ data }) => {
   const { title, content, registration, destination, label } = data;
   return(
       <Row>
           <Col>
               <div className="text-center">
                   <h5>{title}</h5>
                   <h4>{content}</h4>
                   <h5>{registration}</h5>
                   <Link className="btn btn-primary" to={destination}>
                       {label}
                   </Link>
               </div>
           </Col>
       </Row>
   );
}
export default Banner;