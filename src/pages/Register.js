import {useState, useEffect} from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
 
const Register = () => {
 
   const [firstName, setFirstName] = useState("");
   const [lastName, setLastName] = useState("");
   const [address, setAddress] = useState("");
   const [birthday, setBirthday] = useState("");
   const [gender, setGender] = useState("");
   const [contactNo, setContactNo] = useState("");
   const [email, setEmail] = useState("");
   const [password1, setPassword1] = useState("");
   const [password2, setPassword2] = useState("");
   const [shirtSize, setShirtSize] = useState("");
   const [error1, setError1] = useState(true);
   const [error2, setError2] = useState(true);
   const [isActive, setIsActive] = useState(false);
   const [willRedirect, setWillRedirect] = useState(false);
 
   useEffect(() => {
 
       if ((firstName !== '' && lastName !== '' && address !== '' && birthday !== '' && gender !== '' && contactNo !== '' && email !== '' && shirtSize !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
           setIsActive(true);
       } else {
           setIsActive(false);
       }
 
   }, [firstName, lastName, address, birthday, gender, contactNo, shirtSize, email, password1, password2]);
 
   useEffect(() => {
 
       if (firstName === '' || lastName === '' || address === '' || birthday === '' || gender === '' || contactNo === '' || shirtSize === '' || email === '' || password1 === '' || password2 === '') {
           setError1(true);
           setError2(false);
           setIsActive(false);
       } else if ((firstName !== '' && lastName !== '' && address !== '' && birthday !== '' && gender !== '' && contactNo !== '' && shirtSize !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 !== password2)) {
           setError1(false);
           setError2(true);
           setIsActive(false);
       } else if((firstName !== '' && lastName !== '' && address !== '' && birthday !== '' && gender !== '' && contactNo !== '' && shirtSize !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
           setError1(false);
           setError2(false);
           setIsActive(true);
       }
 
   }, [firstName, lastName, address, birthday, gender, contactNo, shirtSize, email, password1, password2]);
 
   const registerUser = (e) => {
 
       e.preventDefault();
 
       fetch(`https://runforearth-be.herokuapp.com/users/register`, {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({
               firstName: firstName,
               lastName: lastName,
               address: address,
               birthday: birthday,
               gender: gender,
               contactNo: contactNo,
               shirtSize: shirtSize,
               email: email,
               password: password1
           })
       })
       .then(res => res.json())
       .then(data => {
 
           if (data === true) {
               Swal.fire({
 
                       icon: "success",
                       title: "Congratulations!",
                       text: "You have successfully registered to the event."
                   });

               setWillRedirect(true);
           } else {
               alert("Something went wrong.");
               setFirstName("");
               setLastName("");
               setAddress("");
               setBirthday("");
               setGender("");
               setContactNo("");
               setShirtSize("");
               setEmail("");
               setPassword1("");
               setPassword2("");
           }
       })
   }
 
   return(
       willRedirect === true ?
               <Navigate to={{pathname: '/login', state: { from: 'register'}}}/>
           :
               <Row className="justify-content-center">
                   <Col xs md="6">
                       <h2 className="text-center my-4">SIGN UP</h2>
                       <p className="text-center my-4">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                       <Card>
                           <Form onSubmit={e => registerUser(e)}>
                               <Card.Body>

                                   <Form.Group controlId="firstName">
                                       <Form.Label>First Name:</Form.Label>
                                       <Form.Control
                                           type="fname"
                                           placeholder="Enter your first name"
                                           value={firstName}
                                           onChange={e => setFirstName(e.target.value)}
                                           required
                                       />
                                   </Form.Group>

                                   <Form.Group controlId="lastName">
                                       <Form.Label>Last Name:</Form.Label>
                                       <Form.Control
                                           type="lname"
                                           placeholder="Enter your last name"
                                           value={lastName}
                                           onChange={e => setLastName(e.target.value)}
                                           required
                                       />
                                   </Form.Group>

                                   <Form.Group controlId="address">
                                       <Form.Label>Address:</Form.Label>
                                       <Form.Control
                                           type="address"
                                           placeholder="Enter your address"
                                           value={address}
                                           onChange={e => setAddress(e.target.value)}
                                           required
                                       />
                                   </Form.Group>                                   

                                   <Form.Group controlId="birthday">
                                       <Form.Label>Birthday:</Form.Label>
                                       <Form.Control
                                           type="bday"
                                           placeholder="MM/DD/YYYY"
                                           value={birthday}
                                           onChange={e => setBirthday(e.target.value)}
                                           required
                                       />
                                   </Form.Group>

                                    <Form.Group controlId="gender">
                                       <Form.Label>Gender:</Form.Label>
                                       <Form.Control
                                           type="gender"
                                           placeholder="M / F"
                                           value={gender}
                                           onChange={e => setGender(e.target.value)}
                                           required
                                       />
                                   </Form.Group>
 
                                    <Form.Group controlId="contactNo">
                                       <Form.Label>Contact Number:</Form.Label>
                                       <Form.Control
                                           type="contact"
                                           placeholder="09xxxxxxxxx"
                                           value={contactNo}
                                           onChange={e => setContactNo(e.target.value)}
                                           required
                                       />
                                   </Form.Group>

                                    <Form.Group controlId="shirtSize">
                                       <Form.Label>Shirt Size:</Form.Label>
                                       <Form.Control
                                           type="shirtSize"
                                           placeholder="XS/ S / M / L / XL / XXL"
                                           value={shirtSize}
                                           onChange={e => setShirtSize(e.target.value)}
                                           required
                                       />
                                   </Form.Group>

                                   <Form.Group controlId="userEmail">
                                       <Form.Label>Email:</Form.Label>
                                       <Form.Control
                                           type="email"
                                           placeholder="Enter your email"
                                           value={email}
                                           onChange={e => setEmail(e.target.value)}
                                           required
                                       />
                                   </Form.Group>
 
                                   <Form.Group controlId="password1">
                                       <Form.Label>Password:</Form.Label>
                                       <Form.Control
                                           type="password"
                                           placeholder="Enter your password"
                                           value={password1}
                                           onChange={e => setPassword1(e.target.value)}
                                           required
                                       />
                                   </Form.Group>
 
                                   <Form.Group controlId="password2">
                                       <Form.Label>Verify Password:</Form.Label>
                                       <Form.Control
                                           type="password"
                                           placeholder="Verify your password"
                                           value={password2}
                                           onChange={e => setPassword2(e.target.value)}
                                           required
                                       />
                                   </Form.Group>
 
                               </Card.Body>
                               <Card.Footer>
                                   {isActive === true ?
                                           <Button
                                               variant="success"
                                               type="submit"
                                               block
                                           >
                                               Register
                                           </Button>
                                       :
                                           error1 === true || error2 === true ?
                                               <Button
                                                   variant="danger"
                                                   type="submit"
                                                   disabled
                                                   block
                                               >
                                                   Please enter your registration details
                                               </Button>
                                           :
                                               <Button
                                                   variant="danger"
                                                   type="submit"
                                                   disabled
                                                   block
                                               >
                                                   Passwords must match
                                               </Button>
                                   }
                               </Card.Footer>
                           </Form>
                       </Card>
                       <p className="text-center mt-3">
                           For Terms and Conditions: <Link to={{pathname: '/login', state: { from: 'register'}}}>Click here.</Link>
                       </p>
                   </Col>
               </Row>
   );
 
}
export default Register;
