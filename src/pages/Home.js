import React from 'react';
import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';
import Header from '../images/header.png';
import Background from '../images/background.png';

 
 
const Home = () => {
   const pageData = {
       title: "A 3K run alongside the candidates of Miss Philippines Earth 2015 to aid the Calumpang River rebabilitation",
       content: "APRIL 25, 2015 | Assembly time: 5:00AM | SM City Batangas Parking Grounds",
       registration: "Registration fee: Php500 inclusive of race kit with shirt",
       destination: "/register",
       label: "Register Now!"
   };

 
   return(
       <React.Fragment>
        <div className = "home">
           <img src={Header} class="center"/>
           <Banner data={pageData}/>
           <Container fluid>
           <img src={Background} className="img-fluid"/>
           </Container>
        </div>
       </React.Fragment>
   );
}
export default Home;